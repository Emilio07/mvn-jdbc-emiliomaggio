package net.tinvention.training.jse.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UtenteDtoTest {
	private UtenteDto u;

	@BeforeEach
	public void init() throws Exception {
		System.out.println("setUp called");
		u = new UtenteDto(1, "Emilio", "Maggio", 0);
	}

	@AfterEach
	public void tearDown() throws Exception {
		System.out.println("tearDown called");
		u = null; // TODO non necessario ...
	}

	@Test
	public void ToStringTest() {
		String string = u.toString(u);
		assertEquals("1 Emilio Maggio 0", string);
	}

}