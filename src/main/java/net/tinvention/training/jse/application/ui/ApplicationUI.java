package net.tinvention.training.jse.application.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.sql.SQLException;
import java.util.List;

import net.tinvention.training.jse.bl.GestioneTimbratureLogic;
import net.tinvention.training.jse.dto.UtenteDto;

public class ApplicationUI {
	private GestioneTimbratureLogic gestioneTimbratureLogic;
	private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	private Logger LOG = LogManager.getLogger(ApplicationUI.class);

	public void add(int number, String s1, String s2) throws NumberFormatException, IOException, SQLException {
		LOG.info("metodo add richiamato a livello UI");
		gestioneTimbratureLogic.aggiungiRigaTabellaUtenti(new UtenteDto(number, s1, s2));
	}

	public void delete(int number) throws NumberFormatException, IOException, SQLException {
		LOG.info("metodo delete richiamato a livello UI");
		gestioneTimbratureLogic.eliminaRigaTabellaUtenti(number);
	}

	public void timbra(int number) throws NumberFormatException, IOException, SQLException {
		gestioneTimbratureLogic.aggiornaTimbratureTabellaUtenti(number);

	}

	public void printAll(List<UtenteDto> lista) throws IOException, SQLException, NullPointerException {
		lista.forEach(u -> System.out.println(u.toString(u)));
	}

	public void start() {
		while (true) {
			System.out.println("###INSERIRE UN COMANDO:");
			String comando = null;
			try {
				comando = in.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				LOG.error("IOException occurred!", e1);
			}
			if (comando.equals("ADD")) {
				System.out.println("Inserimento nuovo utente");
				try {
					System.out.println("MATRICOLA:");
					int n1 = Integer.parseInt(in.readLine());
					System.out.println("NOME:");
					String s1 = in.readLine();
					System.out.println("COGNOME:");
					String s2 = in.readLine();
					add(n1, s1, s2);
				} catch (NumberFormatException | IOException e) {
					LOG.error("NumberFormatException occurred!", e);
				} catch (SQLException e) {
					LOG.error("SQLException occurred!", e);
				}
			} else if (comando.equals("DELETE")) {
				System.out.println("Cancellazione utente");
				try {
					System.out.println("MATRICOLA:");
					int n2 = Integer.parseInt(in.readLine());
					delete(n2);
				} catch (NumberFormatException | IOException e) {
					LOG.error("NumberFormatException occurred!", e);
				} catch (SQLException e) {
					LOG.error("SQLException occurred!", e);
				}
			} else if (comando.equals("TIMBRA")) {
				System.out.println("Timbratura");
				try {
					System.out.println("MATRICOLA:");
					int n3 = Integer.parseInt(in.readLine());
					timbra(n3);
				} catch (NumberFormatException e) {
					LOG.error("NumberFormatException", e);
				} catch (IOException e) {
					LOG.error("IOException occurred!", e);
				} catch (SQLException e) {
					LOG.error("SQLException occurred!", e);
				}
			} else if (comando.equals("PRINT")) {
				System.out.println("Stampa informazioni utente");
				try {
					System.out.println("MATRICOLA(digitare ALL per stampare le informazioni di tutti gli utenti):");
					String s3 = in.readLine();
					if (s3.compareTo("ALL") == 0) {
						printAll(gestioneTimbratureLogic.restituisciListaUtentiTabellaUtenti());
					} else {
						UtenteDto u = gestioneTimbratureLogic.ottieniUtenteTabellaUtenti(Integer.parseInt(s3));
						System.out.println(u.toString(u));
					}
				} catch (IOException e) {
					LOG.error("IOException occurred!", e);
				} catch (SQLException e) {
					LOG.error("SQLException occurred!", e);
				}
			} else if (comando.equals("STOP")) {
				System.out.println("STOP ESECUZIONE PROGRAMMA");
				break;
			} else {
				System.err.println("Comando non recepito o errato");

			}
		}
	}

	public GestioneTimbratureLogic getGestioneRegistroLogic() {
		return gestioneTimbratureLogic;
	}

	public void setGestioneTimbratureLogic(GestioneTimbratureLogic gestioneRegistroLogic) {
		this.gestioneTimbratureLogic = gestioneRegistroLogic;
	}
}

